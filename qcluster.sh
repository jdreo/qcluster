# QCluster.sh
# Licence: AGPL
#
# Collection of shell macros useful for supervision on a SLURM-managed cluster.
#
# To use them, just add `source <path_to>/qcluster.sh` in your favorite shell configuration file.
#
# All command are prefixed with `qc` for ease of discovery
# (think of it as "query cluster", "quality check" or even "queue consistency").

function qchelp()
{
    echo 'QCluster is a collection of shell macros useful for supervision on a SLURM-managed cluster.
All macros are prefixed with "qc" for ease of discovery
(think of it as "query cluster", "quality check" or even "queue consistency").

To get help on a specific macro, use "<the_macro>_help", for example: "qcstatus_help".

Most macros will show the SLURM command used to gather the data (and some other info) on standard error.
' >&2
    qcstatus_help
    qcwatch_help
    qceff_help
    qceffstat_help
}

function _qchelp_colout()
{
    echo '    Note: Depends on the "colout" tool: https://github.com/nojhan/colout
          to color the output. Colout depends on Python, you may need to install it
          and activate a Python module: "module load Python".
' >&2
}

function qcstatus_help()
{
    echo 'qcstatus:
    Detailled state of current SLURM jobs.

    Usage: qcstat <any argument to pass to sacct>
' >&2
    _qchelp_colout
}
function qcstatus()
{
    # FIXME use squeue, which put less load pressure on the slurm manager.
    cmd="sacct --format=jobid,jobname,partition,qos,state,start,end,elapse,reqmem,exitcode $@ | colout -t sacct"
    echo "$cmd" >&2
    $cmd
}

function _qcdate_help()
{
    echo '    Note: "started_date" may be given in plain english
          (e.g. "today", "yesterday", "a month ago", etc.)
          or the ISO format (YYYY[-MM[-HH[THH[:MM[:SS]]]]])
          By default, "started_date" is "today".
' >&2
}
function _qcdate()
{
    qcdate=$(date -I)
    if [[ -z "$1" ]]; then
        # Today by default if no argument.
        qcdate="today"
    else
        # If an argument, attempt at interpreting it as a relative sentence.
        when=$(date -I -d "$today $1")
        if [[ "$?" == "0" ]]; then
            qcdate="$when"
        else
            # Else fallback to passing it directly.
            qcdate="$1"
        fi
    fi
    printf "Since: $qcdate\n" >&2
}


function qcwatch_help()
{
    echo 'qcwatch:
    Permanent view on the state of current SLURM jobs
    since a given date.

    Usage: qcwatch [<started_date>]

    View is refreshed every 10 seconds.

    Use Ctrl-C to end the watch.
' >&2
    _qcdate_help
    _qchelp_colout
}
function qcwatch()
{
    _qcdate "$1"
    local since="$qcdate"
    watch --interval 10 --color "sacct --starttime=$since --format=jobid,state --noheader|grep -v 'ba+'|grep -v 'ex+'|awk '{print \$2}'|sort|uniq -c|sort -n -r|colout -t slurm16"
}


function qceff_help()
{
    echo 'qceff:
    List efficiency statistics of current jobs.

    Usage: qceff [<start_date=today> [<states> [<extra_args>]]]

    If specified, jobs are filtered by "states".
    By default, all jobs are shown.

    "extra_args" are passed to "sacct".

    Example: qceff yesterday COMPLETED,RUNNING,TIMEOUT
' >&2
    _qcdate_help
}
function qceff()
{
    _qcdate "$1"
    local since="$qcdate"

    local states=""
    if [[ "${2-}" ]]; then
        states=" --state=${2} "
    else
        states=""
    fi
    echo "Consider: $states" >&2

    local args="${3-}"
    cmd="reportseff --user $USER --format=+jobname,partition,qos,start,reqmem,maxrss $states --extra-args --starttime=$since $args"
    echo "$cmd" >&2
    $cmd
}


# Convenience function borrowed from Liquid Prompt: htpps://github.com/liquidprompt/liquidprompt
# Makes this whole projet AGPL.
#
# Convert bytes in human-readable, binary-prefixed notation (using powers of 2).
# Precision can be 0, 1 or 2 digits. If not given, precision is two digits.
__lp_bytes_to_human() { # bytes, [precision] -> lp_bytes, lp_bytes_units
    local i="$1" prec="${2:-2}" size remainder=""
    # This has to be split on multiple lines for Zsh 5.0 (see wiki).
    local -a units
    # Not much point in going past PiB, as Bash and Zsh start failing int math.
    units=("B" "KiB" "MiB" "GiB" "TiB" "PiB" "EiB" "YiB" "ZiB")

    for ((size=0; i > 1024 && size < ${#units[@]}-1; size++)); do
        remainder=$((i % 1024 * 100 / 1024))
        i=$((i / 1024))
    done

    if [[ -n $remainder ]]; then
        if ((remainder < 10)); then
            remainder="0$remainder"
        fi
    else
        # Bytes case.
        remainder="00"
    fi

    # Precision formating.
    remainder="${remainder:0:$prec}"
    if [[ -n $remainder ]]; then
        remainder=".$remainder"
    fi

    lp_bytes="$i$remainder"
    lp_bytes_units="${units[$size+$_LP_FIRST_INDEX]}"
}


function _qc_effstat_read_time()
{
    local ftime n tot_t max_t D H M T h m s tm mm st
    ftime="$1"

    n=0
    tot_t=0
    max_t=0
    while IFS='|' read -r tm mm st; do
        if [[ "$tm" == "Elapsed" ]]; then
            continue
        fi
        while IFS=':' read -r h m s; do
            D=${h%-*}
            H=${h#*-}
            if [[ "$D" == "$H" ]]; then
                D=0
            fi
            H=${H#0}
            M=${m#0}
            T=$((H*60 + M))
            if [[ "$T" -gt "$max_t" ]]; then
                max_t="$T"
            fi
            tot_t=$((tot_t + T))
            n=$((n+1))
        done <<<"$tm"
    done <"$ftime"

    if [[ "$n" -eq "0" ]]; then
        echo "ERROR: no time data."
        return 1
    fi

    qc_effstat_read_time_average="$((tot_t/n))"
    qc_effstat_read_time_max="$max_t"
}

function _qc_effstat_read_mem()
{
    local fmem n tot_m max_m tm mm st
    fmem="$1"

    n=0
    tot_m=0
    max_m=0
    while IFS='|' read -r tm mm st; do
        m="${mm%K}" # Remove suffix "K"
        # FIXME check status column for mem overflow.
        if [[ -n "$m" && "$m" != "MaxRSS" ]]; then
            m="$((m*1000))" # reportseff reports Kb.
            if [[ "$m" -gt "$max_m" ]]; then
                max_m="$m"
            fi
            tot_m=$((tot_m + m))
            n=$((n+1))
        fi
    done <"$fmem"

    if [[ "$n" -eq "0" ]]; then
        echo "ERROR: no memory data." >&2
        return 1
    fi

    qc_effstat_read_mem_average="$((tot_m/n))"
    qc_effstat_read_mem_max="$max_m"
}

function qceffstat_help()
{
        echo 'qceffstat:
    Displays summary statistics about the efficiency of current jobs.

    Usage: qceffstat [<started_date=today> [<states=COMPLETED> [<extra_args>]]]

    If specified, jobs are filtered by "states".
    By default, only the COMPLETED jobs are considered.

    "extra_args" are passed to "sacct".

    Example: qceffstat yesterday COMPLETED,RUNNING,TIMEOUT
' >&2
        _qcdate_help
}
function qceffstat()
{
    _qcdate "$1"
    local since="$qcdate"

    local states=""
    if [[ "${2-}" ]]; then
        states=" --state=${2} "
    else
        states=" --state=COMPLETED"
    fi
    echo "Consider: $states" >&2

    local extra_args="${3-}"

    fdata=$(mktemp --suffix="_QC")
    cmd="reportseff --user $USER --format=Elapsed,MaxRSS,State --parsable $states --extra-args --starttime=$since $args"
    echo "$cmd"
    $cmd > $fdata
    echo "Data from: $fdata" >&2

    local n="$(cat $fdata | wc -l)"
    if [[ "${n}" -le 1 ]]; then
        echo "ERROR: no job found." >&2
        return 1
    else
        echo "Found $n jobs." >&2
    fi

    # ftime=$(mktemp --suffix="_QC")
    # cat $fdata | cut -d"|" -f"1" > "$ftime"

    if _qc_effstat_read_time "$fdata"; then
        local avg=$(date -ud "@$(( qc_effstat_read_time_average * 60 ))" +"$(( qc_effstat_read_time_average * 60/3600/24 )) days %H hours %M minutes %S seconds")
        printf "Avg time: $avg\n"
        local max=$(date -ud "@$(( qc_effstat_read_time_max * 60 ))" +"$(( qc_effstat_read_time_max * 60/3600/24 )) days %H hours %M minutes %S seconds")
        printf "Max time: $max\n"
    fi

    # fmem=$(mktemp --suffix="_QC")
    # cat $fdata | cut -d"|" -f"2" > "$fmem"

    if _qc_effstat_read_mem "$fdata"; then
        __lp_bytes_to_human "$qc_effstat_read_mem_average"
        printf "Avg mem: $lp_bytes $lp_bytes_units\n"

        __lp_bytes_to_human "$qc_effstat_read_mem_max"
        printf "Max mem: $lp_bytes $lp_bytes_units\n"
    fi
}

