# QCluster

Collection of shell macros useful for supervision on a SLURM-managed cluster.

To use them, just add `source <path_to>/qcluster.sh` in your favorite shell configuration file (`~/.bashrc` or `~/.zshrc`).

All command are prefixed with `qc` for ease of discovery
(think of it as "query cluster", "quality check" or even "queue consistency").

You can get the list of commands and their help by calling `qchelp`.
